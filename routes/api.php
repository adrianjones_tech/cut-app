<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// reg new and get back passport token
Route::post('user/register', 'API\AuthController@register');

Route::post('create', 'API\PasswordResetController@create');
Route::post('reset', 'API\PasswordResetController@reset');

Route::group(['prefix' => 'user/login', 'middleware' => 'throttle:3,5'], function () {

    Route::post('', 'API\AuthController@login');
    //Route::get('{provider}', 'API\AuthController@redirect')->middleware('web');
    Route::get('{provider}/callback', 'API\AuthController@callback')->middleware('web');
    });

    //Below Routes need a valid Bearer Token

Route::middleware('auth:api')->group( function () {

  // Logout
  Route::get('user/logout', 'API\AuthController@logout');

	// users
	Route::get('users', 		'API\UserController@index');
	Route::get('user/{id}', 	'API\UserController@show');
	Route::put('user/{id}', 	'API\UserController@update');
	Route::delete('user/{id}', 	'API\UserController@destroy');

	// profiles
	Route::get('profile/{id}', 	'API\ProfileController@show');
	Route::post('profile', 		'API\ProfileController@store');
	Route::put('profile/{id}', 	'API\ProfileController@update');

    // services
    Route::post('service',      'API\ServiceController@store');
    Route::put('service/{id}',  'API\ServiceController@update');
    Route::delete('service/{id}',  'API\ServiceController@destroy');

    // ratings
    Route::get('ratings',         'API\RatingController@index');
    Route::post('rating',      'API\RatingController@store');
    Route::put('rating/{id}',  'API\RatingController@update');
    Route::delete('rating/{id}',  'API\RatingController@destroy');

    // comments
    Route::post('rating/{id}/comment',      'API\CommentController@store');
    Route::get('bookings',       'API\BookingController@index');
    Route::get('booking/{id}',     'API\BookingController@show');
    Route::post('booking',       'API\BookingController@store');
    Route::put('booking/{id}',   'API\BookingController@update');
    Route::delete('booking/{id}', 'API\BookingController@destroy');

    // products
    Route::get('products',       'API\ProductController@index');
    Route::get('product/{id}',     'API\ProductController@show');
    Route::post('products',         'API\ProductController@filter');

    // categories
    Route::get('categories',       'API\CategoryController@index');
    Route::get('category/{id}',     'API\CategoryController@show');

    // orders
    Route::post('order',         'API\OrderController@store');
    Route::get('order/{id}',     'API\OrderController@show');
    #Route::put('order/{id}',     'API\OrderController@update');
    Route::delete('order/{id}',  'API\OrderController@destroy');
    // order amandments
    Route::post('order/addItem/{orderId}',  'API\OrderController@addItem');
    Route::put('order/amendItem/{itemId}',  'API\OrderController@amendItem');
    Route::delete('order/amendItem/{itemType}/{itemId}',  'API\OrderController@destroyItem');

    // carts
    Route::get('cart/{userId}',     'API\CartController@show');
    Route::post('cart/addItem/{userId}',  'API\CartController@store');
    Route::put('cart/amendItem/{itemId}',  'API\CartController@update');
    Route::delete('cart/amendItem/{itemId}',  'API\CartController@destroyItem');
    Route::delete('cart/{userId}',  'API\CartController@destroy');

});
