<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // order
    	$faker = \Faker\Factory::create();

        for($i=0; $i<=10; $i++):
            
         DB::table('orders')->insert([           
            'customer_id' => mt_rand(1, 10),
            'vendor_id' => mt_rand(1, 10),
        ]);

         $orderID = $i+1;

        // Items
        // Booking

        DB::table('order_bookings')->insert([
            'price' => mt_rand(1, 25),
            'qty' => mt_rand(1, 5),
            'order_id' => $orderID,
            'booking_id' => mt_rand(1, 10),
        ]);



        // product

         DB::table('order_products')->insert([
            'price' => mt_rand(1, 25),
            'qty' => mt_rand(1, 5),
            'order_id' => $orderID,
            'product_id' => mt_rand(1, 10),
        ]);


        endfor;

    }
}
