<?php

use Illuminate\Database\Seeder;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $faker = \Faker\Factory::create();

        for($i=0; $i<=499; $i++):
            
         DB::table('category_product')->insert([

            'category_id' => mt_rand(1, 13),
            'product_id' => mt_rand(1, 200),
        ]);

        endfor;
    }
}
