<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	// Examples:
 	/*
        $this->call([

        BookingSeeder::class,
        CommentSeeder::class,
        CustomerProfileSeeder::class,
        ProfileSeeder::class,
        RatingSeeder::class,
        ServiceSeeder::class,

        ]);
   	/*
    
    /*
		     DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);
     */

     /*
		`create 50 users and attach a relationship to each user:
		
       factory(App\User::class, 50)->create()->each(function ($user) {
        $user->posts()->save(factory(App\Post::class)->make());
    	});


     */


        $this->call([

        BookingSeeder::class,
        CommentSeeder::class,
        CustomerProfileSeeder::class,
        ProfileSeeder::class,
        RatingSeeder::class,
        ServiceSeeder::class,
        AccountTypesSeeder::class,
        UserSeeder::class,
        CategorySeeder::class,
        ProductSeeder::class,
        CategoryProductSeeder::class,
        OrderSeeder::class,
        CartSeeder::class,

        ]);


    	

    }
}
