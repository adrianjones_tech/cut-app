<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      User::create([
         'name'              => 'kijo',
         'email'             => 'dev@kijo.co',
         'password'          => Hash::make('123456'),
         'email_verified_at' => Carbon::now(),
         'active'            => 1,
      ]);

      // first one admin
      DB::table('users')->insert([
        'name' => 'Kijo Admin',
        'email' => 'admin@ekijo.co',
        'password' => Hash::make('KijoAdmin'),
        'profile_id' => 1,
        'account_type_id' => 1
      ]);

      // 10 Vendors and Customers

      $faker = \Faker\Factory::create();

      for($i=1; $i<=11; $i++){

        $business_name = $faker->word.' '.$faker->word;

        // first one Vendor
        DB::table('users')->insert([
          'name' => $business_name,
          'email' => $faker->email,
          'password' => Hash::make($faker->password(8)),
          'profile_id' => $i,
          'account_type_id' => 2
        ]);

        // profile
        $name =
        DB::table('profiles')->insert([
          'user_id' => $i-1,
          'business_name' => $business_name,
          'contact_number' => '0'.$faker->numberBetween(7700000000, 7999999999),
          'address' => $faker->address,
          'hair_type' => $faker->word,
          'gender' => $faker->word,
          't_and_c' => $faker->paragraph,
        ]);

      }

      for($i=12; $i<=22; $i++) {

        // Next Customer
        $fullName = $faker->firstName.''.$faker->lastName;

        DB::table('users')->insert([
          'name' => $fullName,
          'email' => $faker->email,
          'password' => Hash::make($faker->password(8)),
          'profile_id' => $i,
          'account_type_id' => 3
        ]);

        // profile
        DB::table('customer_profiles')->insert([
          'user_id' => $i-1,
          'full_name' => $fullName,
          'contact_number' => '0'.$faker->numberBetween(7700000000, 7999999999),
          'address' => $faker->address,
          'hair_type' => $faker->word,
          'gender' => $faker->word,
          't_and_c' => 1,
        ]);

        }

      }

}
