<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Gel', 'Wax', 'Mousse', 'Pomade', 'Volumizer', 'Volumising', 'Powders', 'Sprays', 'Thickening', 'Tonics', 'Pastes', 'Putty', 'Clay'];

        foreach ($categories as $category ) {
        
		        DB::table('categories')->insert([
		            
		            'title' => $category

		        ]);
        }

    }
}
