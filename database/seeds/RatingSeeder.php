<?php

use Illuminate\Database\Seeder;

class RatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();

        for($i=0; $i<=10; $i++):
            
         DB::table('ratings')->insert([
            'vendor_id' => mt_rand(1, 10),
            'customer_id' => mt_rand(1, 10),
            'rating' => mt_rand(1, 5),
        ]);

        endfor;
    }
}
