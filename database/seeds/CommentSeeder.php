<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         $faker = \Faker\Factory::create();

        for($i=0; $i<=10; $i++):
            
         DB::table('comments')->insert([
            'comment' => $faker->paragraph,
            'rating_id' => mt_rand(1, 10),
        ]);

        endfor;

    }
}
