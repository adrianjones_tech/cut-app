<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i=0; $i<=200; $i++):
            
         DB::table('products')->insert([
            'name' => $faker->word,
            'price' => mt_rand(1, 25),
        ]);

        endfor;
    }
}
