<?php

use Illuminate\Database\Seeder;

class AccountTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('account_types')->insert([
            'name' => 'Admin',
        ]);
        DB::table('account_types')->insert([
            'name' => 'Vendor',
        ]);
        DB::table('account_types')->insert([
            'name' => 'Customer',
        ]);
    }
}
