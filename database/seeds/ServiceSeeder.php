<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();

        for($i=0; $i<=10; $i++):
            
         DB::table('services')->insert([
            'name' => $faker->firstName.' '.$faker->lastName,
            'price' => mt_rand(1, 10),
            'user_id' => mt_rand(1, 10),
        ]);

        endfor;
    }
}
