
<?php

use Illuminate\Database\Seeder;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       	$faker = \Faker\Factory::create();

        for($i=0; $i<=10; $i++):
            
         DB::table('bookings')->insert([
            'booking_for_date' => $faker->dateTimeBetween('+0 days', '+1 month'),
            'customer_id' => mt_rand(1, 5),
            'vendor_id' => mt_rand(1, 5),
            'vendor_service_id' => mt_rand(1, 5),
        ]);

        endfor;

    }
}
