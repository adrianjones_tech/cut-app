<?php

use Illuminate\Database\Seeder;

class CartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         for($i=0; $i<=10; $i++){
            

	         $userID = $i+1;

	        // Items
	        // Booking

	        DB::table('carts')->insert([
	            'booking_id' => mt_rand(1, 10),
	            'qty' => mt_rand(1, 20),
	            'customer_id' => $userID,
	            'vendor_id' => mt_rand(1, 10),
	        ]);



	        // product

	         DB::table('carts')->insert([
	            'product_id' => mt_rand(1, 10),
	            'qty' => mt_rand(1, 20),
	            'customer_id' => $userID,
	            'vendor_id' => mt_rand(1, 10),
	        ]);

	        DB::table('carts')->insert([
	            'product_id' => mt_rand(1, 10),
	            'qty' => mt_rand(1, 20),
	            'customer_id' => $userID,
	            'vendor_id' => mt_rand(1, 10),
	        ]);
	    }
    }
}
