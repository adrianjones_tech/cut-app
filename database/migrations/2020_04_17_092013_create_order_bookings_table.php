<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_bookings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->constrained()->onDelete('cascade')->nullable();
            $table->integer('booking_id')->unsigned();
            $table->integer('qty')->unsigned();
            $table->float('price')->nullable();
            $table->timestamps();
        });

    /*
        Schema::table('order_bookings', function($table) {
            $table->foreign('order_id')->references('id')->on('orders');
        });

    */    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_bookings');
        
    }
}
