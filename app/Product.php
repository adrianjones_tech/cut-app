<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price',  
    ];

    /**
     * Get the user record of the owner of the profile.
     */
    public function categories()
    {
        //return $this->belongsToMany(Category::class);

        //return $this->belongsToMany('App\Category', 'category_product')->withPivot(['category_id','product_id']);

        //return $this->belongsToMany('App\Category', 'category_product', 'category_id');

        return $this->belongsToMany('App\Category', 'category_product', 'product_id', 'category_id');
    }

}
