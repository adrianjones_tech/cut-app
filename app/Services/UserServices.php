<?php

namespace App\Services;

use App\Models\Transaction;
use App\Models\User;
use App\Models\UserMeta;

class UserServices {

    public static function getBalance($userId) {
        // Get the users latest deposit (which contains latest depositted balance)
        $lastTransaction = Transaction::where([['user_id', $userId], ['transaction_type', '<>', 'WITHDRAW'], ['status', 'APPROVED']])->orderBy('id', 'desc')->first();

        // If the user has not depositted before, then return balance of 0
        if (!$lastTransaction) return 0;

        $withdrawals = Transaction::where([['user_id', $userId], ['transaction_type', 'WITHDRAW']])->orderBy('id', 'desc')->get();

        $pendingWithdrawalAmount = 0;
        foreach ($withdrawals as $wTransaction) {
            // If the withdrawal is older than the latest transaction then continue to the next transaction
            if ($wTransaction->updated_at < $lastTransaction->updated_at) continue;

            // Add the withdrawal amount to the total withdrawal amount to take from the last transaction balance
            $pendingWithdrawalAmount += $wTransaction->transaction_amount;
        }

        // The last depositted balance minus any withdrawal amounts that are newer than the last transaction
        $balance = $lastTransaction->balance - $pendingWithdrawalAmount;

        // $gambleAmount = 0;
        // $gambles = Transaction::where([['user_id', $userId], ['transaction_type', 'ENTRY_FEE']])->orderBy('id', 'desc')->get();
        // foreach ($gambles as $gTransaction) {
        //     // If the gamble is older than the latest transaction then continue to the next transaction
        //     if ($gTransaction->updated_at < $lastTransaction->updated_at) continue;

        //     // Add the gamble amount to the total withdrawal amount to take from the balance
        //     $gambleAmount += $gTransaction->transaction_amount;
        // }

        // return $balance = $balance - $gambleAmount;
        return $balance;
    }

    public function splitName($name): object {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim(preg_replace('#' . $last_name . '#', '', $name));
        return collect(['first_name' => $first_name, 'last_name' => $last_name]);
    }

    public function setUserMeta( $user, $key, $value ): object {
        return UserMeta::updateOrCreate(['user_id' => $user, 'meta_key' => $key], ['meta_key' => $key, 'meta_value' => $value]);
    }
}
