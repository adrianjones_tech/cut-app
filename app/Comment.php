<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Comment extends Model
{
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment', 'rating_id',
    ];

     /**
     * Get the comment record associated with the Rating.
     */
    public function rating()
    {
        return $this->belongsTo('App\Rating', 'id', 'comment_id');
    }  
}
