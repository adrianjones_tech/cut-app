<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    //

    /**
     * Get the user record of the owner of the profile.
     */
    public function user()
    {
        return $this->hasMany('App\User');
    } 

}
