<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'telephone', 'date_of_birth', 'hair_type', 'gender', 't_and_cs_flag', 'business_name', 'address', 'address2', 'city', 'post_code', 't_and_cs', 'provider', 'provider_id', 'profile_id', 'account_type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
        'facebook_uid',
        'google_uid',
        'activation_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the profile record associated with the user.
     */
    public function profile()
    {
        return $this->hasOne('App\Profile', 'id', 'profile_id');
    }    

    /**
     * Get the profile record associated with the user.
     */
    public function accountType()
    {
        return $this->hasOne('App\AccountType', 'id', 'account_type_id');
    }  

    /**
     * Get the services records associated with the user.
     */
    public function service()
    {
        return $this->hasMany('App\Service');
    } 

    /**
     * Get the services records associated with the user.
     */
    public function vendorBooking()
    {
        return $this->hasMany('App\Booking', 'id', 'vendor_id');
    } 

    /**
     * Get the services records associated with the user.
     */
    public function customerBooking()
    {
        return $this->hasMany('App\Booking', 'id', 'customer_id');
    } 
}
