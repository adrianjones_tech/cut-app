<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Order;
use App\OrderBookings;
use App\OrderProducts;
use Illuminate\Http\Request;
use Validator;

class OrderController extends BaseController
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        $orders = Order::all();

       $booking->load('customer', 'vendor', 'products', 'bookings'); // load relationships

        return $this->sendResponse($orders, 'Orders retrieved successfully.');
        */
        // return 201  // Success
         return 403;  // Forbidden
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        /*
        $request->validate([
            'first_name'=>'required',
            //etc
        ]);

        */
        // Get request data
        $data = $request->all();
 
        $order = new Order([
            'vendor_id' => $request->get('vendor_id'),
            'customer_id' => $request->get('customer_id'),          
        ]);

        $order->save();

        // Products
        if(isset($data['product_id'])){
            
            foreach ($data['product_id'] as $key => $value) {
            
                $orderProducts = new OrderProducts([
                    'order_id'      => $order->id,
                    'product_id'    => $data['product_id'][$key],
                    'qty'           => $data['product_qty'][$key],          
                    'price'         => $data['product_price'][$key],       
                    ]);
                
                $orderProducts->save();
            }
        }
        
        
        // Bookings
        if(isset($data['booking_id'])){

            foreach ($data['booking_id']  as $key => $value) {
           
                $orderBookings = new OrderBookings([
                    'order_id'      => $order->id,
                    'booking_id'    => $data['booking_id'][$key],
                    'qty'           => $data['booking_qty'][$key],         
                    'price'         => $data['booking_price'][$key],
                ]);

                $orderBookings->save();
                
            }

        }

        return 201;  // Success
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order =  Order::find($id);

        $order->load('customer', 'vendor', 'products', 'bookings');

        return $this->sendResponse($order, 'Order retrieved successfully.');

        //return 201;  // Success
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $booking = Order::findOrFail($id);
        $booking->update($request->all());

        //return $this->sendResponse($booking, 'Order updsted successfully.');

        return 201;  // Success

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
       // $order->load('products', 'bookings');

        //return $this->sendResponse($order, 'Order retrieved successfully.');

        //var_dump($order);exit;

       // $result = $order->products()->delete();

      //  var_dump($result);exit;

      //  $order->bookings()->delete();

        $order->delete();

        return 201;  // Success
        //return 403;  // Forbidden

    }


        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addItem(Request $request, int $orderId)
    {
        
        $data = $request->all();

                // Products
        if(isset($data['product_id'])){
            
            foreach ($data['product_id'] as $key => $value) {
            
                $orderProducts = new OrderProducts([
                    'order_id'      => $orderId,
                    'product_id'    => $data['product_id'][$key],
                    'qty'           => $data['product_qty'][$key],          
                    'price'         => $data['product_price'][$key],       
                    ]);
                
                $orderProducts->save();
            }
        }
        
        
        // Bookings
        if(isset($data['booking_id'])){

            foreach ($data['booking_id']  as $key => $value) {
           
                $orderBookings = new OrderBookings([
                    'order_id'      => $orderId,
                    'booking_id'    => $data['booking_id'][$key],
                    'qty'           => $data['booking_qty'][$key],         
                    'price'         => $data['booking_price'][$key],
                ]);

                $orderBookings->save();
                
            }

        }

        // return $this->sendResponse($booking, 'Order deleted successfully.');

        return 201;  // Success

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function amendItem(Request $request, $itemId)
    {

        # $booking = Order::findOrFail($id);
        # $booking->update($request->all());

        $data = $request->all();

        // Products
        if(isset($data['product_id'])){

            $orderProducts = OrderProducts::findOrFail($itemId);
            
            //$orderProducts->update($request->all());

            $orderProducts->product_id = $data['product_id'];
            $orderProducts->qty = $data['product_qty'];
            $orderProducts->price = $data['product_price'];
      
            $orderProducts->save();

        }
        
     
        // Bookings
        if(isset($data['booking_id'])){

            $orderBookings = OrderBookings::findOrFail($itemId);      

            $orderBookings->booking_id = $data['booking_id'];
            $orderBookings->qty = $data['booking_qty'];
            $orderBookings->price = $data['booking_price'];
                
          $orderBookings->save();

        }

        return 201;  // Success

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyItem($itemType, $itemId)
    {

        if($itemType == 'product'){

            $product = OrderProducts::findOrFail($itemId);
            $product->delete();  
        }


        if($itemType == 'booking'){

            $booking = OrderBookings::findOrFail($itemId);
            $booking->delete();  
        }

        

        // return $this->sendResponse($booking, 'Order deleted successfully.');

        return 201;  // Success

    }
}
