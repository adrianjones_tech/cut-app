<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Product;
use Illuminate\Http\Request;
use Validator;
use DB;

class ProductController extends BaseController
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = Product::all();
        $products->load('categories');

        //$products->load('profile', 'accountType', 'services');  // load relationships

        return $this->sendResponse($products, 'Products retrieved successfully.');

        // return 201  // Success
        // return 403;  // Forbidden
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        /*
        $request->validate([
            'first_name'=>'required',
            //etc
        ]);
        'comment', 'rating_id',
        */
         /*       
        $comment = new Comment([
            'comment' => $request->get('comment'),
            'rating_id' => $id,          
        ]);
        $comment->save();
        */      
        //return 201;  // Success

        return 403;  // Forbidden
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking =  Product::find($id);

        $booking->load('categories');

        return $this->sendResponse($booking, 'Product retrieved successfully.');

        //return 201;  // Success
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        $booking = Product::findOrFail($id);
        $booking->update($request->all());

        //return $this->sendResponse($booking, 'Product updsted successfully.');

        return 201;  // Success
        */

        return 403;  // Forbidden

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        $booking = Product::findOrFail($id);
        $booking->delete();

        // return $this->sendResponse($booking, 'Product deleted successfully.');

        return 201;  // Success

        */

        return 403;  // Forbidden

    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $filters)
    {
        /*
        // this brings back the products that match, with all of there categories
        $products = Product::With('categories')->WhereHas('categories', function($query) use ($filters) {
                                $query->whereIn('title', $filters->input('categories'));
                            })
                            ->orderBy('id')
                            ->get();               
 
        */

        $products = Product::WhereHas('categories', function($query) use ($filters) {
                                $query->whereIn('title', $filters->input('categories'));
                            })
                            ->offset($filters->input('offset'))
                            ->limit($filters->input('limit'))
                            ->orderBy('id')
                            ->get();


        return $this->sendResponse($products, 'Products retrieved successfully.');        
    }

}
