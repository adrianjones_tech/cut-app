<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\CustomerCustomerProfile;
use Illuminate\Support\Facades\Auth;
use Validator;

class CustomerCustomerProfileController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        $customerProfiles = CustomerProfile::all();

        //return $this->sendResponse($customerProfiles, 'CustomerProfiles retrieved successfully.');

        return 201;  // Success

        */

        return 403;  // Forbidden
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $validator = Validator::make($request->all(), [
           'user_id'  => 'required|integer',
           'full_name'  => 'required|string',
           'contact_number'  => 'required|numeric',
           'hair_type'  => 'required|string',
           'gender'  => 'required|string',
           't_and_c'  => 'required|string',
           'address'  => 'required|string',
       ]);
  
       if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }

        $customerProfile = new CustomerProfile([
            'user_id' => $request->get('user_id'),
            'full_name' => $request->get('full_name'),
            'contact_number' => $request->get('contact_number'),
            'address' => $request->get('address'),
            'hair_type' => $request->get('hair_type'),
            'gender' => $request->get('gender'),
            't_and_c' => $request->get('t_and_c'),
        ]);
        $customerProfile->save();

        //return $this->sendResponse($customerProfile, 'customerProfile retrieved successfully.');

        return 201;  // Success
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customerProfile =  CustomerProfile::find($id);

        $customerProfile->load( 'user');

        return $this->sendResponse($customerProfile, 'customerProfile retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
           'user_id'  => 'required|integer'
       ]);

        
       if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }


        $customerProfile =  CustomerProfile::findOrFail($id);
        $customerProfile->update($request->all());

        //return $this->sendResponse($customerProfile, 'customerProfile updsted successfully.');

        return 201;  // Success
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return 403;  // Forbidden
    }
}
