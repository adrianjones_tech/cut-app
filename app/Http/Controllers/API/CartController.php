<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Cart;
use Illuminate\Http\Request;
use Validator;

class CartController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        $cart = Cart::all();

        $cart->load('profile', 'accountType', 'services');  // load relationships

        return $this->sendResponse($cart, 'Carts retrieved successfully.');
        */
        // return 201  // Success
         return 403;  // Forbidden
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        /*
        $request->validate([
            'first_name'=>'required',
            //etc
        ]);
        'cart', 'rating_id',
        */
        $cart = new Cart([
            'vendor_id' => $request->get('vendor_id'),
            'customer_id' => $request->get('customer_id'),
            'product_id' => $request->get('product_id'),
            'booking_id' => $request->get('booking_id'),
            'qty' => $request->get('qty'),

        ]);
        $cart->save();



        return 201;  // Success
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($userId)
    {

        $cart =  Cart::with('product')->with('booking')->where('customer_id', $userId)->get();

        return $this->sendResponse($cart, 'Cart retrieved successfully.');

        //return 201;  // Success
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $cart = Cart::findOrFail($id);
        $cart->update($request->all());

        //return $this->sendResponse($cart, 'Cart updsted successfully.');

        return 201;  // Success

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($userId)
    {

        $cartItems =  Cart::where('customer_id', $userId)->get();

        foreach ($cartItems as $item) {
            
            //$item->delete();

            //echo "item id = ".$item->id;exit;

            Cart::destroy($item->id);

        }
       

        // return $this->sendResponse($cart, 'Cart deleted successfully.');

        return 201;  // Success

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyItem($id)
    {

        $cartItem = Cart::findOrFail($id);
            
        $cartItem->delete();     

        // return $this->sendResponse($cart, 'Cart deleted successfully.');

        return 201;  // Success

    }
}