<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Profile;
use Illuminate\Support\Facades\Auth;
use Validator;

class ProfileController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        $profiles = Profile::all();

        //return $this->sendResponse($profiles, 'Profiles retrieved successfully.');

        return 201;  // Success

        */

        return 403;  // Forbidden
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $validator = Validator::make($request->all(), [
           'user_id'  => 'required|integer',
           'business_name'  => 'required|string',
           'contact_number'  => 'required|numeric',
           'hair_type'  => 'required|string',
           'gender'  => 'required|string',
           't_and_c'  => 'required|string',
           'address'  => 'required|string',
       ]);
  
       if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }

        $profile = new Profile([
            'user_id' => $request->get('user_id'),
            'business_name' => $request->get('business_name'),
            'contact_number' => $request->get('contact_number'),
            'address' => $request->get('address'),
            'hair_type' => $request->get('hair_type'),
            'gender' => $request->get('gender'),
            't_and_c' => $request->get('t_and_c'),
        ]);
        $profile->save();

        //return $this->sendResponse($profile, 'profile retrieved successfully.');

        return 201;  // Success
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile =  Profile::find($id);

        $profile->load( 'user');

        return $this->sendResponse($profile, 'profile retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
           'user_id'  => 'required|integer'
       ]);

        
       if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }


        $profile =  Profile::findOrFail($id);
        $profile->update($request->all());

        //return $this->sendResponse($profile, 'profile updsted successfully.');

        return 201;  // Success
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return 403;  // Forbidden
    }
}
