<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Booking;
use Illuminate\Support\Facades\Auth;
use Validator;

class BookingController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $bookings = Booking::all();

        //$bookings->load('profile', 'accountType', 'services');  // load relationships

        return $this->sendResponse($bookings, 'Bookings retrieved successfully.');

        // return 201  // Success
        // return 403;  // Forbidden
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        /*
        $request->validate([
            'first_name'=>'required',
            //etc
        ]);
        'comment', 'rating_id',
        */
        // TODO validate date

       $validator = Validator::make($request->all(), [
           'customer_id'        => 'required|integer',
           'vendor_id'          => 'required|integer',
           'vendor_service_id'  => 'required|integer'
       ]);

        
       if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }

        $booking = new Booking([
            'booking_for_date' => $request->get('booking_for_date'),
            'customer_id' => $request->get('customer_id'),
            'vendor_id' => $request->get('vendor_id'),
            'vendor_service_id' => $request->get('vendor_service_id'),        
        ]);
        $booking->save();

        return 201;  // Success
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking =  Booking::find($id);

        $booking->load('profile', 'accountType', 'services');

        return $this->sendResponse($booking, 'Booking retrieved successfully.');

        //return 201;  // Success
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $validator = Validator::make($request->all(), [
           'customer_id'        => 'required|integer',
           'vendor_id'          => 'required|integer',
           'vendor_service_id'  => 'required|integer'
       ]);

        
       if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }

        $booking = Booking::findOrFail($id);
        $booking->update($request->all());

        //return $this->sendResponse($booking, 'Booking updsted successfully.');

        return 201;  // Success

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = Booking::findOrFail($id);
        $booking->delete();

        // return $this->sendResponse($booking, 'Booking deleted successfully.');

        return 201;  // Success

    }

}