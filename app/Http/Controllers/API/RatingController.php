<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Rating;
use Validator;

class RatingController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $ratings = Rating::all();

        //$ratings->load('profile', 'accountType', 'services');  // load relationships

        return $this->sendResponse($ratings, 'ratings retrieved successfully.');

        //return 201; // Success
        // return 403;  // Forbidden
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
           'vendor_id'        => 'required|integer',
           'customer_id'        => 'required|integer',
           'rating'        => 'required|integer',
           
       ]);

        
       if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }

        //echo "into ratings controller.";exit;
        $rating = new Rating([
            'vendor_id' => $request->get('vendor_id'),
            'customer_id' => $request->get('customer_id'),
            'rating' => $request->get('rating'),          
        ]);
        $rating->save();


        return 201;  // Success
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rating =  Rating::find($id);

        return 403;  // Forbidden
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
           'vendor_id'        => 'required|integer',
           'customer_id'        => 'required|integer',
           'rating'        => 'required|integer',
           
       ]);

        if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }


        $rating = Rating::findOrFail($id);
        $rating->update($request->all());

        //return $this->sendResponse($rating, 'rating updsted successfully.');

        return 201;  // Success
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rating = Rating::findOrFail($id);
        $rating->delete();

        // return $this->sendResponse($rating, 'rating deleted successfully.');

        return 201;  // Success
    }
}
