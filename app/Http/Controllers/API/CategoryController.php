<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Category;
use Illuminate\Http\Request;
use Validator;

class CategoryController extends BaseController
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = Category::all();

        //$categories->load('products');  // load relationships

        return $this->sendResponse($categories, 'Categorys retrieved successfully.');

        // return 201  // Success
        // return 403;  // Forbidden
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        /*
        $request->validate([
            'first_name'=>'required',
            //etc
        ]);
        'comment', 'rating_id',
        */
         /*       
        $comment = new Comment([
            'comment' => $request->get('comment'),
            'rating_id' => $id,          
        ]);
        $comment->save();
        */      
        //return 201;  // Success

        return 403;  // Forbidden
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking =  Category::find($id);

        $booking->load('products');

        return $this->sendResponse($booking, 'Category retrieved successfully.');

        //return 201;  // Success
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        $booking = Category::findOrFail($id);
        $booking->update($request->all());

        //return $this->sendResponse($booking, 'Category updsted successfully.');

        return 201;  // Success
        */

        return 403;  // Forbidden

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        $booking = Category::findOrFail($id);
        $booking->delete();

        // return $this->sendResponse($booking, 'Category deleted successfully.');

        return 201;  // Success

        */

        return 403;  // Forbidden

    }

}
