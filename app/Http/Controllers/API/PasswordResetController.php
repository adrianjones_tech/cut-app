<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Mail;
use App\Mail\PasswordReset as PasswordResetMail;
use App\Mail\PasswordResetSuccess;
use App\User;
use App\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
    /**
     * Create Password Reset
     * @bodyParam email string required
     *
     * @param Request $request
     * @group User
     * @return \Illuminate\Http\JsonResponse [string] message
     */
    public function create(Request $request)
    {

        // Validate that email has been provided and is valid
        $request->validate([
            'email' => 'required|string|email',
        ]);

        // Get the user object which matches the email
        $user = User::where('email', $request->email)->first();

        // User not found error 404
        // if (! $user) {
        //     return response()->json([
        //         'message'   => 'Email Address Not Found',
        //         'status'    => 404
        //     ]);
        // }

        if ($user) {
            // Update or create password reset token
            $passwordReset = PasswordReset::updateOrCreate(
                [
                    'email' => $user->email
                ],
                [
                    'email' => $user->email,
                    'token' => Str::random(60)
                ]
            );

            // Notify the user of the password reset request
            if ($user && $passwordReset) {
                Mail::to($user->email)->send(new PasswordResetMail($passwordReset->token));
            }
        }

        // Success
        return response()->json([
            'message'   => 'If the email address has an account associated, then a reset password email will be sent.',
            'status'    => 200,
            // 'token'     => $passwordReset->token
        ]);
    }

    // THIS METHOD CAN LATER BE REMOVED ONCE WE CORRECTLY SETUP THE PASSWORD RESET EMAIL

    /**
     * Get Password Reset
     * @urlParam token string required
     * @param $token
     * @group User
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function find($token)
    {

        // Find password reset by token
        $passwordReset = PasswordReset::where('token', $token)->first();

        // Check if password reset was found
        if (! $passwordReset) {
            return response()->json([
                'message'   => 'This Password Reset Token Is Invalid',
                'status'    => 404
            ]);
        }

        // Ensures that password reset token expires after 1 hour
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(60)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message'   => 'This Password Reset Token Is Invalid',
                'status'    => 404
            ]);
        }

        // Success
        return response()->json([
            'message'   => 'Password Reset Token Data Found Successfully',
            'status'    => 200,
            'data'      => $passwordReset
        ]);
    }

    /**
     * Get User By Password Reset Token
     * @urlParam token string required
     * @param $token
     * @group User
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getUserByPasswordResetToken($token)
    {

        // Find password reset by token
        $passwordResetUserData = PasswordReset::where('token', $token)->first();

        // Check if password reset was found
        if (! $passwordResetUserData) {
            return false;
        }

        // Ensures that password reset token expires after 1 hour
        if (Carbon::parse($passwordResetUserData->updated_at)->addMinutes(60)->isPast()) {
            $passwordResetUserData->delete();
            return false;
        }

        // Return token
        return $passwordResetUserData;
    }

    /**
     * Reset password
     * @bodyParam email string required
     * @bodyParam password string required
     * @bodyParam password_confirmation string required
     * @bodyParam token string required
     * @param Request $request
     * @group User
     * @return \Illuminate\Http\JsonResponse [string] message
     */
    public function reset(Request $request)
    {

        // Validate request data
        $validator = Validator::make(
            $request->all(),
            [
                'password'          => 'required|string|min:8',
                'confirmPassword'   => 'same:password',
                'token'             => 'required|string'
            ]
        );

        // Return error if validation fails
        if ($validator->fails()) {
            return response()->json([
                'message'   => 'Request Validation Failed',
                'status'    => 422,
                'errors'    => $validator->errors()->all()
            ]);
        }

        // Get user email by token
        $userObj = self::getUserByPasswordResetToken($request->token);

        // Get user data was found
        if (! $userObj) {
            return response()->json([
                'message'   => 'An error occurred',
                'status'    => 400,
            ]);
        }

        // Get user email
        $userEmailAddress = $userObj->email;

        // Get password reset
        $passwordReset = PasswordReset::where(
            [
                ['token', $request->token],
                ['email', $userEmailAddress]
            ]
        )->first();

        // Check if password reset token exists/is valid
        if (! $passwordReset) {
            return response()->json([
                'message'   => 'Password Reset Token Is Invalid',
                'status'    => 404
            ]);
        }

        // Get user by email
        $user = User::where('email', $userEmailAddress)->first();

        // Check if user exists
        if (! $user) {
            return response()->json([
                'message'   => 'User Account Not Found',
                'status'    => 404
            ]);
        }

        // Hash and update
        $user->password = Hash::make($request->password);
        $user->save();

        // Delete the password reset token
        $passwordReset->delete();

        // Inform user of password reset success
        Mail::to($user->email)->send(new PasswordResetSuccess());

        // Success
        return response()->json([
            'message'   => 'Password Reset Successfully',
            'status'    => 200,
            'user'      => $user
        ]);
    }
}
