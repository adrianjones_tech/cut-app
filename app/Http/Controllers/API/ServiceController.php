<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Service;
use Validator;

class ServiceController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //$services = Service::all();

       // $services->load('profile', 'accountType');  // load relationships

       // return $this->sendResponse($services, 'Services retrieved successfully.');
         // return 201  // Success
        
        return 403;  // Forbidden
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'user_id'        => 'required|integer',
           'name'        => 'required|string'
       ]);

# TODO validate price
        
       if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }


        $service = new Service([
            'user_id' => $request->get('user_id'),
            'name' => $request->get('name'),
            'price' => $request->get('price'),          
        ]);
        $service->save();


        return 201;  // Success
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 403;  // Forbidden
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
           'user_id'        => 'required|integer',
           'name'        => 'required|string'
       ]);

# TODO validate price
        
       if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }
        
        $service =  Service::findOrFail($id);
        $service->update($request->all());

        //return $this->sendResponse($service, 'service updsted successfully.');

        return 201;  // Success
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::findOrFail($id);
        $service->delete();

         //return $this->sendResponse($service, 'service deleted successfully.');
        return 201;  // Success
    }
}
