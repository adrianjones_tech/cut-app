<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Comment;
use Illuminate\Http\Request;
use Validator;

class CommentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 403;  // Forbidden
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
           'rating_id'        => 'required|integer',
           'comment'        => 'required|string'
       ]);

        
       if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }

        $comment = new Comment([
            'comment' => $request->get('comment'),
            'rating_id' => $id,          
        ]);
        $comment->save();

        return 201;  // Success
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 403;  // Forbidden
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return 403;  // Forbidden
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return 403;  // Forbidden
    }
}
