<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Mail;
use App\Mail\RegisterActivateAccount;
use App\Helpers\Tools;
use App\User;
use App\Profile;
use App\CustomerProfile;
//use App\Models\SelfExcludedUser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use App\Services\UserServices;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{

    use ThrottlesLogins;

    protected $maxAttempts;
    protected $decayMinutes;

    function __construct() {

      // Switch off throttling for local environment
      //if ( config( 'env-values.environment' ) != 'local' ) {
        $this->maxAttempts = 1500; // Default is 5
        $this->decayMinutes = 1; // Default is 1
      //}

    }

    public function login(Request $request)
    {
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        /*
        if ($this->hasTooManyLoginAttempts($request)) {
          $this->fireLockoutEvent($request);
          return $this->sendLockoutResponse($request);
        }
        */

        // Get user by email
        $user = User::where('email', $request->email)->first();

        // Check if user exists
        if ($user) {

            // Check password is correct
            if (Hash::check($request->password, $user->password) && $user->active === 1) {

                // Check if user is admin role
                if ($user->account_type_id === 1) {
                    $token = $user->createToken('Laravel Password Grant Client', ['is-admin'])->accessToken;
                } else {
                    $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                }

                // Update last login
                $data['last_login'] = Carbon::now();
                $user->update($data);

                // Success
                return response()->json([
                    'message'   => 'Logged In Successfully',
                    'status'    => 200,
                    'token'     => $token,
                    'user'      => $user
                ]);
            }

            // Error
           // $this->incrementLoginAttempts($request);
            return response()->json([
                'message'   => 'Incorrect Login Details Or Account Inactive',
                'status'    => 422
            ]);
        }

        // Error
       // $this->incrementLoginAttempts($request);
        return response()->json([
            'message'   => 'Incorrect Login Details',
            'status'    => 422
        ]);
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'username'        => 'required|string|max:255',
            'email'           => 'required|string|email|max:255|unique:users',
            'password'        => 'required|string|min:8',
            'confirmPassword' => 'same:password',
       ]);

       if ($validator->fails()) {

        $error = array("error" => $validator->messages()->first());

            return  json_encode($error);
       }

        // Get request data
        $data = $request->all();

        // profile ID
        if(isset($data['business_name'])){
            // Vendor
            $accountTypeId = 2;
        }
        else{
            // Customer
            $accountTypeId = 3;
        }
        // Create user
        $user = User::create([
            'name'             => $data['username'],
            'email'            => $data['email'],
            'password'         => Hash::make($data['password']),
            'account_type_id'  => $accountTypeId

        ]);


        if(isset($data['business_name'])){
            // Create vendor profile = account_types= 2
            $profile = Profile::create([

            'user_id' => $user->id,
            'business_name' => $data['business_name'],
            'contact_number' => $data['contact_number'],
            'address' => $data['address'],
            'hair_type' => $data['hair_type'],
            'gender' => $data['gender'],
            't_and_c' => $data['t_and_c'],

        ]);

            // Add profile id to user
            $user->update(['profile_id' => $profile->id]);
        }
        else{
            // create customer profile
            $customerProfile = CustomerProfile::create([
            'user_id' => $user->id,
            'full_name' => $data['username'],
            'contact_number' => $data['contact_number'],
            'address' => $data['address'],
            'hair_type' => $data['hair_type'],
            'gender' => $data['gender'],
            't_and_c' => $data['t_and_c'],
        ]);

            // Add profile id to user
            $user->update(['profile_id' => $customerProfile->id]);
        }

        ## TODO add create profile here!!

        // Generate a user auth token
        $token = ['token' => $user->createToken('Cut Password Grant Client')->accessToken];

        // Notify user to confirm account
        if (env('APP_ENV') != 'local') {
            Mail::to($user->email)->send(new RegisterActivateAccount($user->activation_token));
        }

        // Success
        return response()->json([
            'message'   => 'User Registered Successfully',
            'status'    => 200,
            'token'     => $token['token'],
        ]);
    }

    /**
     * Logout
     * @param Request $request
     * @group Authentication
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        // If not request->user() then logout has been called with an incorrect token
        if (!$request->user()) {
            // Success
            return response()->json([
                'message'   => 'Logged Out Successfully (not logged in)',
                'status'    => 200
            ]);
        }

        // Update last login
      //  $data['last_login'] = null;
      //  $request->user()->update($data);

        // Get token
        $token = $request->user()->token();

        // Revoke token
        if ($token) {
            $token->revoke();
        }

        // Success
        return response()->json([
            'message'   => 'Logged Out Successfully',
            'status'    => 200
        ]);
    }

    /**
     * Social Login - Redirect to Provider
     * @urlParam provider string required Facebook, Twitter etc.
     * @param $provider
     * @group Authentication
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Social Login - Callback
     * @urlParam provider string required Facebook, Twitter etc.
     * @param $provider
     * @group Authentication
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function callback($provider)
    {
        try {
            $userSocial = Socialite::driver($provider)->stateless()->user();
        } catch (\Exception $e) {
            return response('That websites auth is not currently supported', 422);
        }
        $user = User::where(['email' => $userSocial->getEmail()])->first();
        if ($user) {
            return response(['token' => $user->createToken('Laravel Password Grant Client')->accessToken], 200);
        }

        $firstName = null;
        $lastName = null;
        $name = null;

        switch ($provider) {
            case 'facebook':
                $formatName = $this->userServices->splitName($userSocial->name);
                $firstName = $formatName['first_name'];
                $lastName = $formatName['last_name'];
                break;
            case 'twitter':
                //needs revisiting when twitter can be tested
                $name = $userSocial->getName();
                break;
            case 'google':
                //needs revisiting when google can be tested
                $firstName = $userSocial->user['name']['givenName'];
                $lastName = $userSocial->user['name']['familyName'];
                break;
            default:
                return response('This type of login is not supported', 422);
        }

        $user = User::create(['name' => $name, 'first_name' => $firstName, 'last_name' => $lastName, 'email' => $userSocial->getEmail(), 'img' => $userSocial->getAvatar(), 'provider_id' => $userSocial->getId(), 'provider' => $provider, 'active' => true,]);

        return response(['token' => $user->createToken('Laravel Password Grant Client')->accessToken], 200);
    }
}
