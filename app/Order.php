<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       
       'vendor_id','customer_id',  
    ];


    /**
     * Get the user record of the owner of the profile.
     */
    public function customer()
    {
        return $this->hasOne('App\User', 'id', 'customer_id');
    }

    /**
     * Get the user record of the owner of the profile.
     */
    public function vendor()
    {
        return $this->hasOne('App\User', 'id', 'vendor_id');
    }

    /**
     * Get the user record of the owner of the profile.
     */
    public function products()
    {
        return $this->belongsToMany('App\Product', 'order_products', 'order_id', 'product_id');
    }

    /**
     * Get the user record of the owner of the profile.
     */
    public function bookings()
    {
        return $this->belongsToMany('App\Booking', 'order_bookings', 'order_id', 'booking_id');
    }
}
