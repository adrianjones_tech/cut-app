<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProducts extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       
       'order_id', 'product_id', 'qty', 'price',  
    ];


   /**
     * Get the order record.
     */
    public function order()
    {
        return $this->belongsToOne('App\Order');
    }

    /**
     * Get the product record of the item of the order.
     */
    public function product()
    {
        return $this->belongsToOne('App\Product');
    }
}
