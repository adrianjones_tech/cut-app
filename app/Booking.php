<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Booking extends Model
{
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_for_date', 'vendor_id', 'customer_id',  'vendor_service_id'
    ];


    // Relationships
     /**
     * Get the comment record associated with the Rating.
     */
    public function vendor()
    {
        return $this->hasOne('App\User', 'id', 'cvendor_id');
    }  

         /**
     * Get the comment record associated with the Rating.
     */
    public function customer()
    {
        return $this->hasOne('App\User', 'id', 'customer_id');
    }  

         /**
     * Get the comment record associated with the Rating.
     */
    public function service()
    {
        return $this->hasOne('App\Service', 'id', 'service_id');
    }  


}
