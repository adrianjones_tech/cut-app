<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerProfile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'full_name', 'contact_number', 'address', 'hair_type', 'gender', 't_and_c', 
    ];

    /**
     * Get the user record of the owner of the profile.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    } 
}
