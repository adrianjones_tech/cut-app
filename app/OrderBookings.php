<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderBookings extends Model
{
   
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       
       'order_id', 'booking_id', 'qty', 'price',  
    ];


   /**
     * Get the order record.
     */
    public function order()
    {
        return $this->belongsToOne('App\Order');
    }

    /**
     * Get the product record of the item of the order.
     */
    public function booking()
    {
        return $this->belongsToOne('App\Booking');
    }
}
