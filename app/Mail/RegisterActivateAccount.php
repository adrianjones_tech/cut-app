<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisterActivateAccount extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct($activation_token)
    {
        $this->activation_token = $activation_token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Duel - Confirm Account')
            ->view('emails.confirm-account')
            ->with(['activation_token' => $this->activation_token])
        ;
    }
}
