<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\Dispute;

class DisputeCreated extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(Dispute $dispute_data)
    {
        $this->dispute_data = $dispute_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Duel - A New Dispute Has Been Created')
            ->view('emails.disputes.created')
            ->with(['dispute_data' => $this->dispute_data])
        ;
    }
}
