<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\Dispute;

class DisputeCreatedSubmitEvidence extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(Dispute $dispute_data)
    {
        $this->dispute_data = $dispute_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Duel - Please Submit Your Evidence')
            ->view('emails.disputes.created-submit-evidence')
            ->with(['dispute_data' => $this->dispute_data])
        ;
    }
}
