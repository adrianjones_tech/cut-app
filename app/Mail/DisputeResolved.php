<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\Dispute;

class DisputeResolved extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(Dispute $dispute_data)
    {
        $this->dispute_data = $dispute_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Duel - Your Dispute Has Been Resolved')
            ->view('emails.disputes.resolved')
            ->with(['dispute_data' => $this->dispute_data])
        ;
    }
}
