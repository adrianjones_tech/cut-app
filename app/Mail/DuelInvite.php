<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\Match;

class DuelInvite extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(Match $matchData)
    {
        $this->matchData = $matchData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Duel - You\'ve Been Challenged')
            ->view('emails.duel.invite')
            ->with(['matchData' => $this->matchData])
        ;
    }
}
