<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Service extends Model
{
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name', 'price', 'user_id'
        
    ];

    /**
     * Get the user record of the owner of the profile.
     */
    public function user()
    {
        return $this->hasOne('App\User');
    } 

    /**
     * Get the user record of the owner of the profile.
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking');
    } 
}
