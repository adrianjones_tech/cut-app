<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Rating extends Model
{
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rating', 'vendor_id', 'customer_id'
    ];

     /**
     * Get the comment record associated with the Rating.
     */
    public function comment()
    {
        return $this->hasMany('App\Comment', 'id', 'comment_id');
    }  
}
