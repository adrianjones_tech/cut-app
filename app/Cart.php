<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       
       'vendor_id','customer_id', 'product_id', 'booking_id', 'qty', 
    ];


    /**
     * Get the itemTyper of the Cart Item.
     */
    public function itemType()
    {
        return $this->hasOne('App\ItemType', 'id', 'item_type_id');
    }


    /**
     * Get the itemTyper of the Cart Item.
     */
    public function product()
    {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }


    /**
     * Get the itemTyper of the Cart Item.
     */
    public function booking()
    {
        return $this->hasOne('App\Booking', 'id', 'booking_id');
    }
}
