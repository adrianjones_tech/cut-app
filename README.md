### Initial Setup

 - Create database in localhost
 - Create .env from .env.example
 - Update .env with your db credentials
 - `php artisan key:generate`
 - `php artisan migrate`
 - `php artisan db:seed`
 - `php artisan migrate:fresh --seed`
 - `php artisan passport:install`
 - `composer dump-autoload`
 - `php artisan cache:clear`
 - `php artisan config:clear`
 - `php artisan config:cache`
